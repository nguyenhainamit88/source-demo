const { DataTypes } = require('sequelize');

module.exports = model;

function model(sequelize) {
  const attributes = {
    name: { type: DataTypes.STRING, allowNull: false },
    description: { type: DataTypes.STRING, allowNull: false },
    link: { type: DataTypes.STRING, allowNull: false },
    playlist_id: { type: DataTypes.STRING, allowNull: false }
  };

  const options = {
    defaultScope: {
      // exclude hash by default
      attributes: { exclude: [] }
    },
    scopes: {
      // include hash with this scope
      withHash: { attributes: {}, }
    }
  };

  return sequelize.define('Video', attributes, options);
}
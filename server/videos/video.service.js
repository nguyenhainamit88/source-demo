const config = require('config.json');
const jwt = require('jsonwebtoken');
const db = require('_helpers/db');

module.exports = {
  getAll,
  getById,
  create,
  update,
  delete: _delete
};

async function getAll() {
  return await db.Video.findAll();
}

async function getById(id) {
  return await getVideo(id);
}

async function create(params) {
  // validate
  if (await db.Video.findOne({ where: { name: params.name } })) {
    throw 'Name "' + params.name+ '" is already taken';
  }
  // save video
  await db.Video.create(params);
}

async function update(id, params) {
  const video = await getVideo(id);

  // copy params to video and save
  Object.assign(video, params);
  await video.save();

  return video.get();
}

async function _delete(id) {
  const video = await getVideo(id);
  await video.destroy();
}

// helper functions

async function getVideo(id) {
  const video = await db.Video.findByPk(id);
  if (!video) throw 'Video not found';
  return video;
}

function omitHash(user) {
  const { hash, ...userWithoutHash } = user;
  return userWithoutHash;
}
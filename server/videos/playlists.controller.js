const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('_middleware/validate-request');
const authorize = require('_middleware/authorize')
const playlistService = require('./playlist.service');
const config = require('config.json');

// routes
router.post('/create', playlistSchema, savePlaylist);
router.get('/', authorize(), getAll);
router.get('/current', authorize(), getCurrent);
router.get('/:id', authorize(), getById);
router.post('/create', playlistSchema, savePlaylist);
// router.put('/:id', authorize(), updateSchema, update);
router.delete('/:id', authorize(), _delete);

module.exports = router;

function playlistSchema(req, res, next) {
  const schema = Joi.object({
    name: Joi.string().required(),
    description: Joi.string().required(),
    image: Joi.string()
  });
  validateRequest(req, next, schema);
}

function savePlaylist(req, res, next) {
  playlistService.create(req.body)
    .then(() => res.json({ message: 'Create playlist successful' }))
    .catch(next);
}

function getAll(req, res, next) {
  playlistService.getAll()
    .then(playlists => res.json(playlists))
    .catch(next);
}

function getCurrent(req, res, next) {
  res.json(req.playlist);
}

function getById(req, res, next) {
  playlistService.getById(req.params.id)
    .then(playlist => res.json(playlist))
    .catch(next);
}

function _delete(req, res, next) {
  playlistService.delete(req.params.id)
    .then(() => res.json({ message: 'Playlist deleted successfully' }))
    .catch(next);
}
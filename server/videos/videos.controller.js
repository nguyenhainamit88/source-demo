const express = require('express');
const router = express.Router();
const Joi = require('joi');
const validateRequest = require('_middleware/validate-request');
const authorize = require('_middleware/authorize')
const videoService = require('./video.service');

// routes
router.post('/create', videoSchema, saveVideo);
router.get('/', authorize(), getAll);
router.get('/current', authorize(), getCurrent);
router.get('/:id', authorize(), getById);
router.put('/:id', authorize(), updateSchema, update);
router.delete('/:id', authorize(), _delete);

module.exports = router;

function videoSchema(req, res, next) {
  const schema = Joi.object({
    name: Joi.string().required(),
    description: Joi.string().required(),
    link: Joi.string().required(),
    playlist_id: Joi.string().required()
  });
  validateRequest(req, next, schema);
}

function saveVideo(req, res, next) {
  videoService.create(req.body)
    .then(() => res.json({ message: 'Create video successful' }))
    .catch(next);
}

function getAll(req, res, next) {
  videoService.getAll()
    .then(videos => res.json(videos))
    .catch(next);
}

function getCurrent(req, res, next) {
  res.json(req.video);
}

function getById(req, res, next) {
  videoService.getById(req.params.id)
    .then(video => res.json(video))
    .catch(next);
}

function updateSchema(req, res, next) {
  const schema = Joi.object({
    firstName: Joi.string().empty(''),
    lastName: Joi.string().empty(''),
    username: Joi.string().empty(''),
    password: Joi.string().min(6).empty('')
  });
  validateRequest(req, next, schema);
}

function update(req, res, next) {
  videoService.update(req.params.id, req.body)
    .then(video => res.json(video))
    .catch(next);
}

function _delete(req, res, next) {
  videoService.delete(req.params.id)
    .then(() => res.json({ message: 'Video deleted successfully' }))
    .catch(next);
}
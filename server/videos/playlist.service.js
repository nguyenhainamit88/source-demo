const db = require('_helpers/db');

module.exports = {
  getAll,
  getById,
  create,
  update,
  delete: _delete
};

async function getAll() {
  return await db.Playlist.findAll();
}

async function getById(id) {
  return await getPlaylist(id);
}

async function create(params) {
  // validate
  if (await db.Playlist.findOne({ where: { name: params.name } })) {
    throw 'Name "' + params.name+ '" is already taken';
  }
  // save playlist
  await db.Playlist.create(params);
}

async function update(id, params) {
  const playlist = await getPlaylist(id);

  // copy params to video and save
  Object.assign(playlist, params);
  await playlist.save();

  return playlist.get();
}

async function _delete(id) {
  const playlist = await getPlaylist(id);
  await playlist.destroy();
}

// helper functions
async function getPlaylist(id) {
  const playlist = await db.Playlist.findByPk(id);
  if (!playlist) throw 'Playlist is not found';
  return playlist;
}
const { DataTypes } = require('sequelize');

module.exports = model;

function model(sequelize) {
  const attributes = {
    session_id: { type: DataTypes.STRING, allowNull: false },
    amount_total: { type: DataTypes.STRING, allowNull: false },
    currency: { type: DataTypes.STRING, allowNull: false },
    customer: { type: DataTypes.STRING, allowNull: false },
    customer_email: { type: DataTypes.STRING, allowNull: true },
    mode: { type: DataTypes.STRING, allowNull: false },
    payment_intent: { type: DataTypes.STRING, allowNull: false },
    payment_method_types: { type: DataTypes.STRING, allowNull: false },
    payment_status: { type: DataTypes.STRING, allowNull: false }
  };

  const options = {
    defaultScope: {
      // exclude hash by default
      attributes: { exclude: [] }
    },
    scopes: {
      // include hash with this scope
      withHash: { attributes: {}, }
    }
  };

  return sequelize.define('Order', attributes, options);
}
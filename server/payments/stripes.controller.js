const express = require('express');
const router = express.Router();
const stripeService = require('./stripe.service');


// routes
router.post('/create-checkout-session', async (req, res, next) => {
  const result = await stripeService.create(req.body);
  res.json(result);
});

router.get('/checkout', async (req, res, next) => {
  let sessionId = req.query.session_id;
  const result = await stripeService.getSessionStripe(sessionId);

  // Save session into database to report purpose.
  stripeService.trackingCheckout(result);

  res.json(result);
});

module.exports = router;
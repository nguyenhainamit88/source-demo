const config = require('config.json');
const db = require('_helpers/db');
// Set your secret key. Remember to switch to your live secret key in production!
// See your keys here: https://dashboard.stripe.com/account/apikeys
const stripe = require('stripe')(config.stripeSecureKey);

async function create(params) {
  const query = {
    payment_method_types: ['card'],
    line_items: [
      {
        price_data: {
          currency: 'usd',
          product_data: {
            name: params.name,
          },
          unit_amount: params.amount*100,
        },
        quantity: 1,
      },
    ],
    mode: 'payment',
    success_url: `${config.urlFrontEndApp}/payment/success?session_id={CHECKOUT_SESSION_ID}`,
    cancel_url: `${config.urlFrontEndApp}/payment/failure`
  };
  console.log("params", JSON.stringify(params));
  console.log("query", JSON.stringify(query));
  const session = await stripe.checkout.sessions.create(query);
  return { id: session.id };
}

async function trackingCheckout(data) {
  // save order
  data.payment_method_types = data.payment_method_types.toString();
  data.session_id = data.id;
  delete data.id;

  // validate
  if (await db.Order.findOne({ where: { session_id: data.session_id } })) {
    throw 'Name "' + data.session_id+ '" is already taken';
  }

  return await db.Order.create(data);
}

async function getSessionStripe(sessionId) {
  return await stripe.checkout.sessions.retrieve(
    sessionId
  );
}

module.exports = {
  create,
  getSessionStripe,
  trackingCheckout
};

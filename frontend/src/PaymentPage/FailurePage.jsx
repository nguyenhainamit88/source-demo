import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { stripeActions } from '../_actions';

function FailurePage () {

  const dispatch = useDispatch();

  const handleClickPayment = (e) => {
    // dispatch(stripeActions.create());
  };

  return (<>
      <div className="row">
        <div className="col-lg-12">
          <h1>ERROR:</h1>
          <p>Something went wrongs. Contact with the admin site to reslove this problems.</p>
        </div>
      </div>
    </>
  );
}

export { FailurePage };
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { stripeActions } from '../_actions';

function ReturnPage () {

  const dispatch = useDispatch();

  return (<>
      <div className="row">
        <div className="col-lg-12">
          <h1>ReturnPage</h1>
        </div>
      </div>
    </>
  );
}

export { ReturnPage };
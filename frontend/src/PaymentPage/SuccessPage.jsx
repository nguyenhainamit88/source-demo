import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { stripeActions } from '../_actions';
import { useQuery } from "../_hooks";

function SuccessPage () {

  const dispatch = useDispatch();

  const query = useQuery();

  useEffect(() => {
    dispatch(stripeActions.getSessionStrip(query.get('session_id')));
  });

  return (<>
      <div className="row">
        <div className="col-lg-12">
          <h1>Thank you for your tip. We promise that we are trying to create a lot content like this for you.</h1>
        </div>
      </div>
    </>
  );
}

export { SuccessPage };
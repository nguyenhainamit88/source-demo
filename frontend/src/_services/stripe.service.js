import config from 'config';
import { loadStripe } from '@stripe/stripe-js';
// Make sure to call `loadStripe` outside of a component’s render to avoid
// recreating the `Stripe` object on every render.
const stripePromise = loadStripe(config.stripePublicKey);

export const stripeService = {
  create,
  sessionStrip
};

async function sessionStrip(sessionId) {
  const requestOptions = {
    method: 'GET'
  };
  // Call your backend to create the Checkout Session
  const response = await fetch(`${config.apiUrl}/payments/checkout?session_id=${sessionId}`, requestOptions);

  const session = await response.json();

  return session;
}

async function create(amount) {

  // Get Stripe.js instance
  const stripe = await stripePromise;
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(amount)
  };
  // Call your backend to create the Checkout Session
  const response = await fetch(`${config.apiUrl}/payments/create-checkout-session`, requestOptions);

  const session = await response.json();

  // When the customer clicks on the button, redirect them to Checkout.
  const result = await stripe.redirectToCheckout({
    sessionId: session.id,
  });

  if (result.error) {
    // If `redirectToCheckout` fails due to a browser or network
    // error, display the localized error message to your customer
    // using `result.error.message`.
    alert(result.error.message);
  }

  return result;
}
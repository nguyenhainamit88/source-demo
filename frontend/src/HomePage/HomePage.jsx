import React, { useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { playlistActions } from '../_actions';

function HomePage() {

    const dispatch  = useDispatch();
    const playlists = useSelector(state => state.playlists.items);

    useEffect(() => {
        dispatch(playlistActions.getAll());
    }, []);

    return (
        <div className="col-lg-8 offset-lg-2">
          {playlists && playlists.map((playlist) => (
            <p><Link key={playlist.id} to={"/playlist/" + playlist.id}>{playlist.name}</Link></p>
          ))}
        </div>
    );
}

export { HomePage };
import { playlistConstants } from '../_constants';
import { playlistService } from '../_services';
import { alertActions } from './';
import { history } from '../_helpers';

export const playlistActions = {
  getAll
};

function getAll() {
  return dispatch => {
    dispatch(request());

    playlistService.getAll()
      .then(
        data => dispatch(success(data)),
        error => dispatch(failure(error.toString()))
      );
  };

  function request() { return { type: playlistConstants.GETALL_REQUEST } }
  function success(data) { return { type: playlistConstants.GETALL_SUCCESS, data } }
  function failure(error) { return { type: playlistConstants.GETALL_FAILURE, error } }
}
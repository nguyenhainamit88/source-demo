import { stripeConstants } from '../_constants';
import { stripeService } from '../_services';

export const stripeActions = {
  create,
  getSessionStrip,
};

function getSessionStrip(sessionId) {
  return dispatch => {
    dispatch(request());

    stripeService.sessionStrip(sessionId)
      .then(
        session => dispatch(success(session)),
        error => dispatch(failure(error.toString()))
      );
  };

  function request() { return { type: stripeConstants.GET_SESSION_REQUEST } }
  function success(stripe) { return { type: stripeConstants.GET_SESSION_SUCCESS, stripe } }
  function failure(error) { return { type: stripeConstants.GET_SESSION_FAILURE, error } }
}

function create(data) {
  return dispatch => {
    dispatch(request());

    stripeService.create(data)
      .then(
        session => dispatch(success(session)),
        error => dispatch(failure(error.toString()))
      );
  };

  function request() { return { type: stripeConstants.CREATE_REQUEST } }
  function success(stripe) { return { type: stripeConstants.CREATE_SUCCESS, stripe } }
  function failure(error) { return { type: stripeConstants.CREATE_FAILURE, error } }
}
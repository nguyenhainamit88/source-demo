import { videoConstants } from '../_constants';
import { videoService } from '../_services';

export const videoActions = {
  getAll,
  getByPaylistId,
  getById,
};

function getAll() {
  return dispatch => {
    dispatch(request());

    videoService.getAll()
      .then(
        data => dispatch(success(data)),
        error => dispatch(failure(error.toString()))
      );
  };

  function request() { return { type: videoConstants.GETALL_REQUEST } }
  function success(data) { return { type: videoConstants.GETALL_SUCCESS, data } }
  function failure(error) { return { type: videoConstants.GETALL_FAILURE, error } }
}

function getByPaylistId(playlistId) {
  return dispatch => {
    dispatch(request());

    videoService.getByPlaylistId(playlistId)
      .then(
        data => dispatch(success(data)),
        error => dispatch(failure(error.toString()))
      );
  };

  function request() { return { type: videoConstants.GET_VIDEO_BY_PLAYLIST_ID_REQUEST } }
  function success(data) { return { type: videoConstants.GET_VIDEO_BY_PLAYLIST_ID_SUCCESS, data } }
  function failure(error) { return { type: videoConstants.GET_VIDEO_BY_PLAYLIST_ID_FAILURE, error } }
}

function getById(videoId) {
  return dispatch => {
    dispatch(request());

    videoService.getById(videoId)
      .then(
        data => dispatch(success(data)),
        error => dispatch(failure(error.toString()))
      );
  };

  function request() { return { type: videoConstants.GET_VIDEO_BY_ID_REQUEST } }
  function success(data) { return { type: videoConstants.GET_VIDEO_BY_ID_SUCCESS, data } }
  function failure(error) { return { type: videoConstants.GET_VIDEO_BY_ID_FAILURE, error } }
}
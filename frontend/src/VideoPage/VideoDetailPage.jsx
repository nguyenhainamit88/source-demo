import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { videoActions, stripeActions, playlistActions } from '../_actions';

function VideoDetailPage(req) {
  const videoId = req.match.params.id;
  const dispatch = useDispatch();
  const playlists = useSelector(state => state.playlists.items);
  const video = useSelector(state => state.videos.item);
  const [ tips, setTips ] = useState(0);

  useEffect(() => {
    dispatch(playlistActions.getAll());
    dispatch(videoActions.getById(videoId));
  }, []);

  const handleClickPayment = (e) => {
    dispatch(stripeActions.create({
      amount: tips,
      name: video.name
    }));
  };

  function displayPlaylistName(playlistId)
  {
    const playlist = playlists.find(playlist => playlist.id === parseInt(playlistId));
    return playlist && playlist.name;
  }

  const handleTipsAmount = (e) => {
    setTips(e.target.value);
  };

  return (<>
      <div className="row">
        <div className="col-lg-12">
          <nav aria-label="breadcrumb">
            <ol className="breadcrumb custom-breadcrumb">
              <li className="breadcrumb-item">
                <a href="/">Home</a>
              </li>
              <li className="breadcrumb-item">
                {video && (
                  <a href={"/playlist/" + video.playlist_id} target="_blank">{displayPlaylistName(video.playlist_id)}</a>
                )}
              </li>
              <li className="breadcrumb-item active" aria-current="page">{video && video.name}</li>
            </ol>
          </nav>
        </div>
      </div>
      <div className="row custom-row">
        <div className="col-lg">
          <iframe width="560" height="315" src={video && video.link} frameBorder="0"
                  allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                  allowFullScreen></iframe>
        </div>
      </div>
      <div className={'row custom-row'}>
        <div className={'col-lg-10'}>
          <input type={'number'} name={'tips-amount'} className={'tips-amount'} placeholder={5} min={5} step={5} onChange={handleTipsAmount}/>
        </div>
        <div className={'col-lg-2'}>
          <button className="btn-tip" onClick={handleClickPayment} type="button">
            <i className="fas fa-donate"></i> Tip the Spirits
          </button>
        </div>
      </div>
    </>
  );
}

export { VideoDetailPage };
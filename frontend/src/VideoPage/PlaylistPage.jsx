import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {Link} from "react-router-dom";
import { playlistActions, videoActions } from "../_actions";

function PlaylistPage() {

  const dispatch = useDispatch();
  const videos = useSelector(state => state.videos.items);

  useEffect(() => {
    dispatch(videoActions.getAll());
  }, []);

  return (<>
      <h4>List video of playlist:</h4>
      <div className={'row'}>
        <div className="col-lg-8 offset-lg-2">
          {videos && videos.map((video) => (
            <p><Link key={video.id} to={"/video/" + video.id}>{video.name}</Link></p>
          ))}
        </div>
      </div>
    </>
  );
}

export { PlaylistPage };
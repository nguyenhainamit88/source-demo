import { videoConstants } from '../_constants';

export function videos(state = {
  items: [],
  loading: false
}, action) {
  switch (action.type) {
    case videoConstants.GETALL_REQUEST:
      return {
        loading: true
      };
    case videoConstants.GETALL_SUCCESS:
      return {
        items: action.data
      };
    case videoConstants.GETALL_FAILURE:
      return {
        error: action.error
      };

    case videoConstants.GET_VIDEO_BY_ID_REQUEST:
      return {
        loading: true
      };
    case videoConstants.GET_VIDEO_BY_ID_SUCCESS:
      return {
        item: action.data
      };
    case videoConstants.GET_VIDEO_BY_ID_FAILURE:
      return {
        error: action.error
      };

    default:
      return state
  }
}
import { stripeConstants } from '../_constants';

export function stripes(state = {}, action) {
  switch (action.type) {
    case stripeConstants.CREATE_REQUEST:
      return {
        loading: true
      };
    case stripeConstants.CREATE_SUCCESS:
      return {
        session_id: action.id
      };
    case stripeConstants.CREATE_FAILURE:
      return {
        error: action.error
      };
    case stripeConstants.GET_SESSION_REQUEST:
      return {
        loading: true
      };
    case stripeConstants.GET_SESSION_SUCCESS:
      return {
        session: action.stripe
      };
    case stripeConstants.GET_SESSION_FAILURE:
      return {
        error: action.error
      };
    default:
      return state
  }
}
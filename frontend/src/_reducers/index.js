import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import { users } from './users.reducer';
import { alert } from './alert.reducer';
import { stripes } from './stripes.reducer';
import { playlists } from './playlist.reducer';
import { videos } from "./videos.reducer";

const rootReducer = combineReducers({
    authentication,
    registration,
    users,
    alert,
    stripes,
    playlists,
    videos
});

export default rootReducer;
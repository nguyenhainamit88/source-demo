import { playlistConstants } from '../_constants';

export function playlists(state = {
  items: [],
  loading: false
}, action) {
  switch (action.type) {
    case playlistConstants.GETALL_REQUEST:
      return {
        loading: true
      };
    case playlistConstants.GETALL_SUCCESS:
      return {
        items: action.data
      };
    case playlistConstants.GETALL_FAILURE:
      return {
        error: action.error
      };
    default:
      return state
  }
}
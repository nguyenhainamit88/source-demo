# Configs API server node 
Jump inside folder `/backend`:

Find file config: `/server/config.json`

``
{
    "database": {
        "host": "localhost",
        "port": 3306,
        "user": "root",
        "password": "p5HeKuz6",
        "database": "videos"
    },
    "secret": "THIS IS USED TO SIGN AND VERIFY JWT TOKENS, REPLACE IT WITH YOUR OWN SECRET, IT CAN BE ANY STRING",
    "urlFrontEndApp": "http://localhost:8080",
    "stripeSecureKey": "sk_test_51Hrn4sHox9Tw1I9JxvmEi71fN0sLgLPoKRhPsdqTxTD2p9vWiIGiM0GF0ZgSWVT0exX4idooH4NMshmWmjRB1Cke00iNh9wu93"
}
``

Create a database name: ``videos`` or any name it depends on you. If you want to have a data example, let's import file /videos.sql. If not, source code will automatically generate table for themself.

Run ``node server.js`` to start server

# Configs Backend management
Jump inside folder `/backend`:

Find file config: `webpack.config.js` to config URL of API server if it's different: `http://localhost:4000`.

# Configs Frontend
Jump inside folder `/frontend`:

Find file config: `webpack.config.js` to config URL of API server if it's different: `http://localhost:4000`.

import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import {videoActions, playlistActions} from '../_actions';

function VideosPage() {
    const [video, setVideo] = useState({
        name: '',
        description: '',
        link: '',
        playlist_id: ''
    });
    const [submitted, setSubmitted] = useState(false);
    const loading = useSelector(state => state.videos.loading);
    const playlists = useSelector(state => state.playlists.items);
    const videos = useSelector(state => state.videos.items);
    const dispatch = useDispatch();
    // reset login status
    useEffect(() => {
        dispatch(playlistActions.getAll());
        dispatch(videoActions.getAll());
    }, []);

    function handleChange(e) {
        const { name, value } = e.target;
        setVideo(video => ({ ...video, [name]: value }));
    }
      function handleDeleteVideo(id) {
        dispatch(videoActions.delete(id));
      }
    function handleSubmit(e) {
        e.preventDefault();
        setSubmitted(true);
        if (video.name && video.description && video.link && video.playlist_id) {
            dispatch(videoActions.create(video));
            dispatch(videoActions.getAll());
        }
    }

    function displayPlaylistName(playlistId)
    {
      const playlist = playlists.find(playlist => playlist.id === parseInt(playlistId));
      return playlist && playlist.name;
    }

    return (
        <div className="col-lg-8 offset-lg-2">
            <h2>Videos</h2>
            <form name="form" onSubmit={handleSubmit}>
                <div className="form-group">
                  <label>Playlist</label>
                  <select name="playlist_id" onChange={handleChange} className={'form-control'} >
                    <option value={""}>{""}</option>
                    {playlists && playlists.map((playlist) => (
                      <option value={playlist.id}>{playlist.name}</option>
                    ))}
                  </select>
                  {submitted && !video.playlist_id &&
                  <div className="invalid-feedback">Playlist is required</div>
                  }
                </div>
                <div className="form-group">
                    <label>Name</label>
                    <input type="text" name="name" value={video.name} onChange={handleChange} className={'form-control' + (submitted && !video.name ? ' is-invalid' : '')} />
                    {submitted && !video.name &&
                        <div className="invalid-feedback">Name is required</div>
                    }
                </div>
                <div className="form-group">
                    <label>Description</label>
                    <input type="text" name="description" value={video.description} onChange={handleChange} className={'form-control' + (submitted && !video.description ? ' is-invalid' : '')} />
                    {submitted && !video.description &&
                        <div className="invalid-feedback">Description is required</div>
                    }
                </div>
                <div className="form-group">
                    <label>Link</label>
                  <textarea type="text" name="link" value={video.link} onChange={handleChange} className={'form-control' + (submitted && !video.link ? ' is-invalid' : '')} />
                    {submitted && !video.link &&
                        <div className="invalid-feedback">Link is required</div>
                    }
                </div>
                <div className="form-group">
                    <button className="btn btn-primary">
                        {loading && <span className="spinner-border spinner-border-sm mr-1"></span>}
                        Save
                    </button>
                    <Link to="/login" className="btn btn-link">Cancel</Link>
                </div>
            </form>
              <table className="table">
                <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Name</th>
                  <th scope="col">Playlist</th>
                  <th scope="col">Description</th>
                  <th scope="col">Link</th>
                  <th scope="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                {videos && videos.map((item) => (
                  <tr>
                    <th scope="row">{item.id}</th>
                    <td>{item.name}</td>
                    <td>{displayPlaylistName(item.playlist_id)}</td>
                    <td>{item.description}</td>
                    <td>{item.link}</td>
                    <td><a onClick={() => handleDeleteVideo(item.id)} className="text-primary">Delete</a></td>
                  </tr>
                ))}


                </tbody>
              </table>
        </div>
    );
}

export { VideosPage };
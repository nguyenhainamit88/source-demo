import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import {playlistActions} from '../_actions';

function PlaylistPage() {
  const [playlist, setPlaylist] = useState({
    name: '',
    description: '',
    image: ''
  });
  const [submitted, setSubmitted] = useState(false);
  const loading = useSelector(state => state.playlists.loading);
  const playlists = useSelector(state => state.playlists.items);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(playlistActions.getAll());
  }, []);

  function handleChange(e) {
    const { name, value } = e.target;
    setPlaylist(playlist => ({ ...playlist, [name]: value }));
  }
  function handleDeletePlaylist(id) {
    dispatch(playlistActions.delete(id));
  }
  function handleSubmit(e) {
    e.preventDefault();

    setSubmitted(true);
    if (playlist.name) {
      dispatch(playlistActions.create(playlist));
      dispatch(playlistActions.getAll());
    }
  }

  return (
    <div className="col-lg-8 offset-lg-2">
      <h2>Playlists</h2>
      <form name="form" onSubmit={handleSubmit}>
        <div className="form-group">
          <label>Name</label>
          <input type="text" name="name" value={playlist.name} onChange={handleChange} className={'form-control' + (submitted && !playlist.name ? ' is-invalid' : '')} />
          {submitted && !playlist.name &&
          <div className="invalid-feedback">Name is required</div>
          }
        </div>
        <div className="form-group">
          <label>Description</label>
          <input type="text" name="description" value={playlist.description} onChange={handleChange} className={'form-control' + (submitted && !playlist.description ? ' is-invalid' : '')} />
          {submitted && !playlist.description &&
          <div className="invalid-feedback">Description is required</div>
          }
        </div>
        <div className="form-group">
          <label>Image</label>
          <textarea type="text" name="image" value={playlist.image} onChange={handleChange} className={'form-control'} />
        </div>
        <div className="form-group">
          <button className="btn btn-primary">
            {loading && <span className="spinner-border spinner-border-sm mr-1"></span>}
            Save
          </button>
          <Link to="/login" className="btn btn-link">Cancel</Link>
        </div>
      </form>
      <table className="table">
        <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Name</th>
          <th scope="col">Description</th>
          <th scope="col">Image</th>
          <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        {playlists && playlists.map((item) => (
          <tr>
            <th scope="row">{item.id}</th>
            <td>{item.name}</td>
            <td>{item.description}</td>
            <td><img src={item.image} alt={item.name} /> </td>
            <td><a onClick={() => handleDeletePlaylist(item.id)} className="text-primary">Delete</a></td>
          </tr>
        ))}


        </tbody>
      </table>
    </div>
  );
}

export { PlaylistPage };
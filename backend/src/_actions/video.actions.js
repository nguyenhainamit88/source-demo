import {videoConstants} from '../_constants';
import {videoService} from '../_services';
import {history} from "../_helpers";
import {alertActions} from "./alert.actions";

export const videoActions = {
  create,
  getAll,
  delete: _delete
};

function create(video) {
  return dispatch => {
    dispatch(request());
    console.log("video", video);
    videoService.create(video)
      .then(() => {
          dispatch(success(video));
          history.push('/videos');
          dispatch(alertActions.success('Create video successful'));
        },
        error => {
          dispatch(failure(error.toString()));
          dispatch(alertActions.error(error.toString()));
        }
      );
  };

  function request() { return { type: videoConstants.CREATE_REQUEST } }
  function success(video) { return { type: videoConstants.CREATE_SUCCESS, video } }
  function failure(error) { return { type: videoConstants.CREATE_FAILURE, error } }
}

function getAll() {
  return dispatch => {
    dispatch(request());

    videoService.getAll()
      .then(
        videos => dispatch(success(videos)),
        error => dispatch(failure(error.toString()))
      );
  };

  function request() { return { type: videoConstants.GETALL_REQUEST } }
  function success(videos) { return { type: videoConstants.GETALL_SUCCESS, videos } }
  function failure(error) { return { type: videoConstants.GETALL_FAILURE, error } }
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
  return dispatch => {
    dispatch(request(id));

    videoService.delete(id)
      .then(
        video => dispatch(success(id)),
        error => dispatch(failure(id, error.toString()))
      );
  };

  function request(id) { return { type: videoConstants.DELETE_REQUEST, id } }
  function success(id) { return { type: videoConstants.DELETE_SUCCESS, id } }
  function failure(id, error) { return { type: videoConstants.DELETE_FAILURE, id, error } }
}
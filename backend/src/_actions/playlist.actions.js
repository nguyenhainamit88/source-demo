import {playlistConstants} from '../_constants';
import {playlistService} from '../_services';
import {alertActions} from "./alert.actions";

export const playlistActions = {
  create,
  getAll,
  delete: _delete
};

function create(playlist) {
  return dispatch => {
    dispatch(request(playlist));

    playlistService.create(playlist)
      .then(() => {
          dispatch(success());
          dispatch(alertActions.success('Create playlist successful'));
        },
        error => {
          dispatch(failure(error.toString()));
          dispatch(alertActions.error(error.toString()));
        }
      );
  };

  function request(playlist) { return { type: playlistConstants.CREATE_REQUEST, playlist } }
  function success(playlist) { return { type: playlistConstants.CREATE_SUCCESS, playlist } }
  function failure(error) { return { type: playlistConstants.CREATE_FAILURE, error } }
}

function getAll() {
  return dispatch => {
    dispatch(request());

    playlistService.getAll()
      .then(
        playlists => dispatch(success(playlists)),
        error => dispatch(failure(error.toString()))
      );
  };

  function request() { return { type: playlistConstants.GETALL_REQUEST } }
  function success(playlists) { return { type: playlistConstants.GETALL_SUCCESS, playlists } }
  function failure(error) { return { type: playlistConstants.GETALL_FAILURE, error } }
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
  return dispatch => {
    dispatch(request(id));

    playlistService.delete(id)
      .then(
        playlist => dispatch(success(id)),
        error => dispatch(failure(id, error.toString()))
      );
  };

  function request(id) { return { type: playlistConstants.DELETE_REQUEST, id } }
  function success(id) { return { type: playlistConstants.DELETE_SUCCESS, id } }
  function failure(id, error) { return { type: playlistConstants.DELETE_FAILURE, id, error } }
}
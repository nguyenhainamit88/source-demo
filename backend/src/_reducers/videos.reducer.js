import { videoConstants } from '../_constants';

export function videos(state = {
  loading: false,
  videos: []
}, action) {
  switch (action.type) {
    case videoConstants.GETALL_REQUEST:
      return {
        loading: true
      };
    case videoConstants.GETALL_SUCCESS:
      return {
        items: action.videos
      };
    case videoConstants.GETALL_FAILURE:
      return {
        error: action.error
      };
    case videoConstants.DELETE_REQUEST:
      // add 'deleting:true' property to user being deleted
      return {
        ...state,
        items: state.items.map(video =>
          video.id === action.id
            ? { ...video, deleting: true }
            : video
        )
      };
    case videoConstants.DELETE_SUCCESS:
      // remove deleted user from state
      return {
        items: state.items.filter(video => video.id !== action.id)
      };
    case videoConstants.DELETE_FAILURE:
      // remove 'deleting:true' property and add 'deleteError:[error]' property to user
      return {
        ...state,
        items: state.items.map(video => {
          if (video.id === action.id) {
            // make copy of user without 'deleting:true' property
            const { deleting, ...videoCopy } = video;
            // return copy of user with 'deleteError:[error]' property
            return { ...videoCopy, deleteError: action.error };
          }

          return video;
        })
      };
    default:
      return state
  }
}
import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import { users } from './users.reducer';
import { videos } from './videos.reducer';
import { playlists } from './playlists.reducer';
import { alert } from './alert.reducer';
const rootReducer = combineReducers({
    authentication,
    registration,
    users,
    videos,
    alert,
    playlists
});

export default rootReducer;
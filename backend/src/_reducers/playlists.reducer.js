import { playlistConstants } from '../_constants';

export function playlists(state = {
  loading: false,
  playlists: []
}, action) {
  switch (action.type) {
    case playlistConstants.GETALL_REQUEST:
      return {
        loading: true
      };
    case playlistConstants.GETALL_SUCCESS:
      return {
        items: action.playlists
      };
    case playlistConstants.GETALL_FAILURE:
      return {
        error: action.error
      };
    case playlistConstants.DELETE_REQUEST:
      // add 'deleting:true' property to user being deleted
      return {
        ...state,
        items: state.items.map(playlist =>
          playlist.id === action.id
            ? { ...playlist, deleting: true }
            : playlist
        )
      };
    case playlistConstants.DELETE_SUCCESS:
      // remove deleted user from state
      return {
        items: state.items.filter(playlist => playlist.id !== action.id)
      };
    case playlistConstants.DELETE_FAILURE:
      // remove 'deleting:true' property and add 'deleteError:[error]' property to user
      return {
        ...state,
        items: state.items.map(playlist => {
          if (playlist.id === action.id) {
            // make copy of user without 'deleting:true' property
            const { deleting, ...playlistCopy } = playlist;
            // return copy of user with 'deleteError:[error]' property
            return { ...playlistCopy, deleteError: action.error };
          }

          return playlist;
        })
      };
    default:
      return state
  }
}